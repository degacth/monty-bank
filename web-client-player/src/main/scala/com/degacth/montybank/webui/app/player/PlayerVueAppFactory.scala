package com.degacth.montybank.webui.app.player

import com.degacth.mantybank.webui.vue.BaseComponent
import com.degacth.montybank.messages.Move
import com.degacth.montybank.utils.PriceFormat

import scalajs.js._

abstract class PlayerVueApp extends BaseComponent {
  var sm: PlayerStateManager = _
  var moveDoneId: String = _
  var moveResultMessage: String = _
  val onSuccess: ThisFunction
  val onDeposit: ThisFunction
  val onMoveTimeout: ThisFunction
}

object PlayerVueAppFactory extends PlayerVueApp {
  type C = PlayerVueApp

  override val template: String =
    <div class="uk-container application" v-bind:class="{'game-active': isGameActive}">
      <app-header v-if="username" v-bind:username="username"></app-header>

      <section class="actions">
        <div class="monty-hall actions-panel">
          <div class="game-field uk-box-shadow-large">
            <transition enter-active-class="animated bounceIn" mode="out-in">
              <div v-if="isActiveMove" key="monty-hall">
                <app-monty-hall v-bind:move="move"
                                v-bind:onMoveTimeout="onMoveTimeout"
                                v-bind:onFailed="onFailed"
                                v-bind:onSuccess="onSuccess"></app-monty-hall>
              </div>

              <div v-else-if="moveResultMessage" class="move-result-message uk-text-center" key="move-message">
                [[ moveResultMessage ]]
              </div>
            </transition>
          </div>
        </div>

        <div class="bank actions-panel">
          <div class="money">
            <h1 class="uk-heading-line uk-text-center">
              <span class="player-account">[[ account | toPrice ]]</span>
            </h1>
          </div>
          <app-account-manager v-bind:account="account"
                               v-bind:onDeposit="onDeposit">
          </app-account-manager>
        </div>
      </section>
    </div> toString

  override val onSuccess: ThisFunction = (self: C, move: Move) => {
    self.moveDoneId = move.id
    self.sm.successMove(move)
    self.moveResultMessage = s"You WIN ${PriceFormat.toPrice(move.price)}"
  }

  override val onDeposit: ThisFunction = (self: C, sum: Int) => self.sm.deposit(sum)
  val onFailed: ThisFunction = (self: C, move: Move) => {
    self.moveDoneId = move.id
    self.moveResultMessage = s"You LOSE"
  }

  override val onMoveTimeout: ThisFunction = (self: C, move: Move) => {
    self.moveDoneId = move.id
    self.moveResultMessage = "Timeout"
  }

  override def data(): Dynamic = Dynamic.literal(
    moveDoneId = "",
    moveResultMessage = "Getting ready",
  )

  override val computed: Dynamic = Dynamic.literal(
    username = { self: C => self.sm.state.player.name }: ThisFunction,
    isGameActive = { self: C => self.sm.isGameActive }: ThisFunction,
    move = { self: C => self.sm.state.move.orNull }: ThisFunction,
    account = { self: C => self.sm.state.account }: ThisFunction,
    isActiveMove = { self: C => self.sm.state.move.exists(_.id != self.moveDoneId) }: ThisFunction,
  )

  override val props: Array[String] = Array("sm")

  override val methods: Dynamic = Dynamic.literal(
    onSuccess = onSuccess,
    onDeposit = onDeposit,
    onFailed = onFailed,
    onMoveTimeout = onMoveTimeout,
  )

  override val watch: Dynamic = Dynamic.literal(
    move = { self: C =>
      self.moveResultMessage = ""
    }: ThisFunction,
  )
}
