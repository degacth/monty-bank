package com.degacth.montybank.webui.app.player

import com.degacth.mantybank.webui.lodash.__
import com.degacth.mantybank.webui.vue.BaseComponent
import com.degacth.montybank.messages.Move

import scala.util.Random
import scalajs.js._
import org.scalajs.dom
import JSConverters._

abstract class MontyHallComponent extends BaseComponent {
  var move: Move = _
  val setMove: ThisFunction
  var open: ThisFunction = _
  var doors: Array[Door] = Array()
  var moveTimeoutId: Int = _
  var firstTryTimeoutId: Int = _
  def onSuccess(move: Move): Unit = ???
  def onFailed(move: Move): Unit = ???
  def onMoveTimeout(move: Move): Unit = ???
}

class MontyHallFactory(showWrongTimeout: Int, delayForMessage: Int = 0) extends MontyHallComponent {
  type C = MontyHallComponent

  val rand = new Random()
  val doorsCount = 3

  val template: String =
    <div class="options uk-child-width-1-1 uk-flex-center uk-child-width-1-3@s uk-text-center uk-grid-collapse"
         uk-grid="">

      <div v-for="(door, i) in doors"
           class="door uk-border-circle animated"
           v-on:click="open(door)"
           v-bind:class="{selected: door.selected, opened: door.opened, success: door.isSuccess, pulse: door.opened || door.selected}">

        <div class="uk-border-pill"></div>
      </div>
    </div> toString

  override val setMove: ThisFunction = (self: C) => {
    val stepTimeout = self.move.timeout + self.move.ts - Date.now().toLong - delayForMessage
    self.moveTimeoutId = __.delay(() => stopAfterMoveTimeout.call(self), stepTimeout.toInt)

    val m = self.move
    val successIndex = m.indexType match {
      case "auto" => rand.nextInt(3)
      case index => index.toInt
    }

    self.doors = ((0 until doorsCount) map { i: Int =>
      new Door(i == successIndex, false, false)
    }).toJSArray

    self.open = firstTryHandler
  }

  val stopAfterMoveTimeout: ThisFunction = (self: C) => {
    self.onMoveTimeout(self.move)
  }

  val emptyHandler: ThisFunction = (_: Any) => ()

  val firstTryHandler: ThisFunction = (self: C, door: Door) => {
    door.selected = true
    self.open = emptyHandler
    self.firstTryTimeoutId = __.delay(() => {
      val wrongDoors = self.doors.filter(d => !d.success && !d.selected)
      wrongDoors(rand.nextInt(wrongDoors.length)).opened = true
      self.open = secondTryHandler
    }, showWrongTimeout)
  }

  val secondTryHandler: ThisFunction = (self: C, door: Door) => {
    if (!door.opened) {
      door.opened = true
      self.open = emptyHandler
      if (door.success) self.onSuccess(self.move)
      else self.onFailed(self.move)
    }
  }

  val onMove: ThisFunction = (self: C) => setMove.call(self)

  override val props: Array[String] = Array("move", "onSuccess", "onFailed", "onMoveTimeout")

  override val methods: Dynamic = Dynamic.literal(
    setMove = setMove, open = emptyHandler
  )

  override def data(): Dynamic = Dynamic.literal(
    doors = doors,
    firstTryTimeoutId = 0,
    moveTimeoutId = 0,
  )

  val created: ThisFunction = (self: C) => self.setMove.call(self)

  override val watch: Dynamic = Dynamic.literal(
    move = onMove
  )

  val beforeDestroy: ThisFunction = (self: C) => {
    dom.window.clearInterval(self.moveTimeoutId)
    dom.window.clearInterval(self.firstTryTimeoutId)
  }
}

class Door(val success: Boolean, var opened: Boolean, var selected: Boolean) extends Object {
  def isSuccess: Boolean = success && opened
}
