package com.degacth.montybank.webui.app.player

import com.degacth.mantybank.webui.vue.Vue

import scalajs.js._

object ComponentRegistry {
  lazy val registry: Unit = {
    val components = Map(
      "app-header" -> HeaderComponent,
      "app-monty-hall" -> new MontyHallFactory(1000, 600),
      "app-account-manager" -> AccountManagerFactory,
    )

    components foreach {
      case (name: String, component: Any) => Vue.component(name, component)
    }
  }
}
