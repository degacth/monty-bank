package com.degacth.montybank.webui.app.player

import com.degacth.mantybank.webui.vue.BaseComponent
import scala.scalajs.js._

object HeaderComponent extends BaseComponent {
  override val template: String =
    <header>
      <h2 class="uk-heading-line uk-text-center">
        <span class="username">[[ username ]]</span>
      </h2>
    </header> toString

  override val props = Array("username")
}
