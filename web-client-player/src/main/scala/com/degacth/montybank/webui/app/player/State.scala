package com.degacth.montybank.webui.app.player

import com.degacth.mantybank.webui.state.{State, StateManager, WsManager}
import com.degacth.montybank.messages
import com.degacth.montybank.messages.{Accepted, ClientErrorMessage, ConnectionMessage, Deposit, Move, NextMove, Player, StartGame, Success}
import org.scalajs.dom

trait PlayerState extends State {
  var player: Player
  var gameStarted = false
  var move: Option[Move] = None
  var account = 0
}

trait PlayerStateManager extends StateManager {
  override val state: PlayerState

  def isGameActive: Boolean = state.gameStarted
  def nextMove(move: Move): Unit = state.move = Some(move)
  def successMove(move: Move): Unit = state.account += move.price
  def deposit(n: Int): Unit = ???
  def startGame(): Unit = {
    state.gameStarted = true
    state.account = 0
  }
}

class WsPlayerStateManager(override val state: PlayerState) extends PlayerStateManager with WsManager {
  override val url: String = s"ws://${dom.window.location.host}/ws/player/${state.player.id}"

  override def onMessage(msg: ConnectionMessage): Unit = msg match {
    case StartGame => startGame()
    case NextMove(move) => nextMove(move)
    case Accepted(playerState: messages.PlayerState) => state.account = playerState.account
    case ClientErrorMessage(msg) => dom.console.error("Client error", msg.toString)
    case m: Any => dom.console.error("Not supported message", m.toString)
  }

  override def successMove(move: Move): Unit = send(Success(move))
  override def deposit(n: Int): Unit = send(Deposit(n))

  init()
}
