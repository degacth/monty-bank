package com.degacth.montybank.webui

import com.degacth.mantybank.webui.WebApp
import com.degacth.mantybank.webui.vue._
import com.degacth.montybank.messages.{Accepted, Player}
import com.degacth.montybank.webui.app.player._
import org.scalajs.dom
import org.scalajs.dom.XMLHttpRequest
import org.scalajs.dom.ext.Ajax

import scala.concurrent.{ExecutionContext, Future}
import scala.scalajs.js._
import scala.util.{Failure, Success, Try}

object WebUI extends WebApp {

  import com.degacth.montybank.messages.ConnectionMessagesOpts._

  implicit val ec: ExecutionContext = concurrent.ExecutionContext.Implicits.global

  private val playerIdKey = "playerId"

  def init(container: dom.Element): Unit = {
    val playerResult = (dom.window.localStorage.getItem(playerIdKey) match {
      case id: String => Ajax.get(s"/players/$id")
      case null => Ajax.post("/players")
    }) collect onPlayerResult

    playerResult onComplete {
      case Success(Right(player)) =>
        dom.window.localStorage.setItem(playerIdKey, player.id)
        initVueApp(container, player)
      case Success(Left(msg)) =>
        showError(msg)

      case Failure(err) =>
        dom.window.localStorage.removeItem(playerIdKey)
        showError(err.toString)
    }
  }

  private def initVueApp(container: dom.Element, p: Player): Unit = {
    ComponentRegistry.registry
    val sm = new WsPlayerStateManager(new PlayerState {
      override var player: Player = p
    })
    val app = Vue extend PlayerVueAppFactory

    Dynamic.newInstance(app.asInstanceOf[Dynamic]) {
      Dynamic.literal(
        el = container,
        propsData = Dynamic.literal(sm = sm.asInstanceOf[Any])
      )
    }
  }

  private val onPlayerResult: PartialFunction[dom.XMLHttpRequest, Either[String, Player]] = {
    case resp => resp.responseText.fromStr match {
      case Right(Accepted(player: Player)) => Right(player)
      case Right(other) => Left(s"Wrong answer $other")
      case Left(error) => Left(error.toString)
    }
  }

  private def showError(msg: String) = dom.window alert msg
}
