package com.degacth.montybank.webui.app.player

import com.degacth.mantybank.webui.lodash.__
import com.degacth.mantybank.webui.vue.BaseComponent

import scala.scalajs.js._

abstract class AccountManagerComponent extends BaseComponent {
  var account: Int = _
  var onDeposit: ThisFunction = _
  var toDepositSum: Int = _
  var updateDeposit: ThisFunction
}

object AccountManagerFactory extends AccountManagerComponent {
  type C = AccountManagerComponent
  override val template: String =
    <div class="account-manager">
      <div class="apply-to-bank uk-text-center uk-margin-bottom">
        <button class="uk-button uk-button-danger uk-button-large uk-width-1-2 to-deposit submit-deposit"
                v-bind:disabled="!toDepositSum"
                v-on:click="onDepositSubmit()">
          [[ toDepositSum | toPrice ]]
        </button>
      </div>

      <div class="sender-control uk-text-center">
        <div class="uk-button-group">
          <button class="uk-button uk-button-secondary decrease" v-on:click="updateDeposit(-1)">
            <span uk-icon="reply"></span>
          </button>
          <button class="uk-button uk-button-primary increase" v-on:click="updateDeposit(1)">
            <span uk-icon="forward"></span>
          </button>
        </div>
      </div>
    </div> toString

  override val props: Array[String] = Array("account", "onDeposit")

  val onDepositSubmit: ThisFunction = (self: C) => {
    self.onDeposit.call(null, self.toDepositSum)
    self.toDepositSum = 0
  }

  override def data(): Dynamic = Dynamic.literal(
    toDepositSum = 0,
  )

  override var updateDeposit: ThisFunction = (self: C, n: Int) => {
    val toDeposit = self.toDepositSum + n
    self.toDepositSum = __.clamp(toDeposit, 0, self.account)
  }

  override val methods: Dynamic = Dynamic.literal(
    updateDeposit = updateDeposit,
    onDepositSubmit = onDepositSubmit,
  )
}
