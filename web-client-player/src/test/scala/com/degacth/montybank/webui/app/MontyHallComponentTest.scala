package com.degacth.montybank.webui.app

import com.degacth.mantybank.webui.lodash.__
import com.degacth.mantybank.webui.sinon.Sinon
import com.degacth.montybank.messages.Move
import com.degacth.montybank.webui.app.player.MontyHallFactory

import scala.concurrent.Future
import scala.scalajs.js._

class MontyHallComponentTest extends BaseSpec {
  var po: MontyHallPageObject = _
  val firstDoor = 0
  val successDoor = 1
  val lastDoor = 2
  var onSuccessResult: Move = _
  var onMoveTimeout: Sinon = _
  val moveTimeout = 50

  override def componentLiterals: (Object, Object) = (new MontyHallFactory(0), Dynamic.literal(
    move = Move("id", 10, Date.now().toLong, moveTimeout, "1").asInstanceOf[Any],
    onSuccess = (result: Move) => {
      onSuccessResult = result
    },
    onFailed = () => ???,
    onMoveTimeout = onMoveTimeout,
  ))

  override def beforeEach(): Unit = {
    onMoveTimeout = Sinon.fake()
    super.beforeEach()
    po = new MontyHallPageObject(this)
  }

  def runAfter[T](timeout: Int)(body: => T): Future[T] = {
    new Promise((res: Function, _) => {
      __.delay(() => {
        res.call(null)
      }, timeout)
    }).toFuture map { _: Any =>
      body
    }
  }

  "Monty Hall" must {
    "show closed doors" in {
      assert(po.doorsCount === 3)
      assert(po.openedCount === 0)
    }

    "show other opened door" in {
      po.openDoor(firstDoor)
      po.openDoor(successDoor)
      po.openDoor(lastDoor)

      runAfter(1) {
        assert(po.isOpened(firstDoor) === false)
        assert(po.isSelected(firstDoor) === true)
        assert(po.isOpened(lastDoor) === true)
        assert(po.isSuccess(successDoor) === false)
      }
    }

    "show success door" in {
      po.openDoor(firstDoor)

      runAfter(1) {
        po.openDoor(successDoor)
      } map { _ =>
        assert(po.isSuccess(successDoor) === true)
        assert(onSuccessResult.id === "id")
      }
    }

    "should emit on move timeout" in {
      runAfter(moveTimeout + 10) {
        assert(onMoveTimeout.called)
      }
    }
  }
}

class MontyHallPageObject(spec: BaseSpec) {
  def doors = spec.byCssAll(".door")
  def doorsCount = doors.length
  def openedCount = spec.byCssAll(".opened").length
  def openDoor(n: Int) = spec.byCssAll(".door")(n).click()
  def selectedCount = spec.byCssAll(".selected").length
  def successCount = spec.byCssAll(".success").length
  def isOpened = hasDoorClass(_, "opened")
  def isSelected = hasDoorClass(_, "selected")
  def isSuccess = hasDoorClass(_, "success")

  private def hasDoorClass(n: Int, klass: String) = doors(n).classList.contains(klass)
}
