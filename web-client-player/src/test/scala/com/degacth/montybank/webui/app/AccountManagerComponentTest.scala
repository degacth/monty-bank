package com.degacth.montybank.webui.app

import com.degacth.mantybank.webui.sinon.Sinon
import com.degacth.montybank.utils.PriceFormat
import com.degacth.montybank.webui.app.player.AccountManagerFactory
import scala.scalajs.js._

class AccountManagerComponentTest extends BaseSpec {
  var accountManager: AccountManagerPO = _
  var onDeposit: Sinon = _
  val account = 10
  val toDeposit = 4

  override def componentLiterals: (Object, Object) = (
    AccountManagerFactory,
    Dynamic.literal(
      account = account,
      onDeposit = onDeposit,
    )
  )

  override def beforeEach(): Unit = {
    onDeposit = Sinon.fake()
    super.beforeEach()
    accountManager = new AccountManagerPO(this)
  }

  "Account manager" must {
    "emit deposit with selected sum" in {
      accountManager.increaseDeposit(toDeposit)
      inFuture {
        assert(accountManager.toDeposit === toDeposit)
      }
    }

    "limit upper bound" in {
      accountManager.increaseDeposit(account + 2)
      inFuture {
        assert(accountManager.toDeposit === account)
      }
    }

    "limit lower bound" in {
      accountManager.increaseDeposit(2)
      accountManager.decreaseDeposit(4)
      inFuture {
        assert(accountManager.toDeposit === 0)
      }
    }

    "not call on deposit callback" in {
      accountManager.sendDeposit()
      inFuture {
        assert(onDeposit.notCalled)
      }
    }

    "call on deposit" in {
      val deposit = 2
      accountManager.increaseDeposit(2)

      inFuture {
        accountManager.sendDeposit()
      } map { _ =>
        assert(onDeposit.calledOnce)
        assert(onDeposit.calledWith(deposit))
      }
    }
  }
}

class AccountManagerPO(spec: BaseSpec) {
  val increaseDeposit: Int => Unit = clickDepositButtons(".increase")
  val decreaseDeposit: Int => Unit = clickDepositButtons(".decrease")

  def toDeposit: Int = PriceFormat.fromPrice(spec.byCss(".to-deposit").textContent)
  def sendDeposit(): Unit = spec.byCss(".submit-deposit").click()

  private def clickDepositButtons(buttonSelector: String)(times: Int): Unit = {
    val increaseButton = spec.byCss(buttonSelector)
    (0 until times) foreach (_ => increaseButton.click())
  }
}
