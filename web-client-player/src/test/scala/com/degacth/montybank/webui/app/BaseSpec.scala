package com.degacth.montybank.webui.app

import com.degacth.mantybank.webui.vue.ComponentSpec
import org.scalatest.BeforeAndAfterEach
import org.scalatest.wordspec.AsyncWordSpecLike
import scala.concurrent.ExecutionContext

trait BaseSpec extends AsyncWordSpecLike with BeforeAndAfterEach with ComponentSpec {
  override implicit val executionContext: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  override def beforeEach(): Unit = {
    prepare()
    super.beforeEach()
  }
}
