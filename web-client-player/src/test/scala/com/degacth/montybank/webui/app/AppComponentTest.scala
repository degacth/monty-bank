package com.degacth.montybank.webui.app

import com.degacth.montybank.messages.{Move, Player}
import com.degacth.montybank.utils.PriceFormat
import com.degacth.montybank.webui.app.player._

import scala.concurrent.Future
import scala.scalajs.js._

class AppComponentTest extends BaseSpec {

  ComponentRegistry.registry
  var player: PlayerClientPO = _
  val username = "Mr Test Userovich"
  var sm: PlayerStateManager = _

  val movePrice = 10
  val testMove: Move = Move("", movePrice, 0, 0, "1")

  override def componentLiterals: (Object, Object) = (PlayerVueAppFactory, Dynamic.literal(sm = sm.asInstanceOf[Any]))

  override def beforeEach(): Unit = {
    sm = new PlayerStateManager {
      override val state: PlayerState = new PlayerState {
        override var player: Player = Player("", username)
        move = Some(testMove)
      }
    }
    super.beforeEach()
    player = new PlayerClientPO(this)
  }

  "Player App" must {
    "show player name" in {
      val $username = player.name
      assert(!$username.isEmpty)
      assert($username == username)
    }

    "update player account" in {
      assert(player.account === 0)

      sm.successMove(testMove)
      Future.unit map { _ =>
        assert(player.account === testMove.price)
      }
    }

    "show move result label" in {
      val testMessage = "test message"
      component.$data.moveResultMessage = testMessage

      Future.unit map { _ =>
        assert(player.resultMessage === testMessage)
      }
    }

    "reset game after started game" in {
      sm.successMove(testMove)

      Future.unit map { _ =>
        assert(player.account === testMove.price)
        sm.startGame()
      } map { _ =>
        assert(player.account === 0)
      }
    }
  }
}

class PlayerClientPO(spec: BaseSpec) {
  def name: String = spec.byCss(".username").textContent
  def account: Int = PriceFormat.fromPrice(spec.byCss(".player-account").textContent.trim)
  def resultMessage: String = spec.byCss(".move-result-message").textContent.trim
}
