package com.degacth.montybank.messages

object ConnectionMessagesOpts {

  import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

  implicit class MessageToStr(msg: ConnectionMessage) {
    def toStr: String = msg.asJson.noSpaces
  }

  implicit class MessageFromString(msg: String) {
    def fromStr: Either[Error, ConnectionMessage] = decode[ConnectionMessage](msg)
  }
}

case class Player(id: String, name: String) extends Acceptable
case class Move(id: String, price: Int, ts: Long, timeout: Int, indexType: String) extends Successful

sealed trait ConnectionMessage {
  val payload: Any
}

sealed trait EmptyPayloadMessage extends ConnectionMessage {
  override val payload: Any = null
}

sealed trait ClientError
sealed trait Acceptable
sealed trait Successful

case object StartGame extends EmptyPayloadMessage

case class NextMove(payload: Move) extends ConnectionMessage
case class Deposit(payload: Int) extends ConnectionMessage with Acceptable

case class Success(payload: Successful) extends ConnectionMessage with Acceptable
case class Accepted(payload: Acceptable) extends ConnectionMessage
case class GameResults(payload: List[GameResult]) extends ConnectionMessage
case class PlayerState(account: Int = 0, deposit: Int = 0) extends Acceptable {
  def canDeposit(n: Int): Boolean = account - n >= 0
}

case class ParseErrorMessage(payload: String) extends ConnectionMessage
case class ClientErrorMessage(payload: ClientError) extends ConnectionMessage

case class NoSuchMove(moveId: String) extends ClientError
case object DepositExceeded extends ClientError

case class GameResult(player: String, account: Int, deposit: Int)
