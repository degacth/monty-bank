package com.degacth.montybank.utils

object PriceFormat {
  def toPrice(n: Int) = s"$n$$"
  def fromPrice(price: String): Int = price.trim.replace("$", "").toInt
}
