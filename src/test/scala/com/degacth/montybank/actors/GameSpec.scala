package com.degacth.montybank.actors

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestActors, TestKit}
import com.degacth.montybank.messages.{Deposit, GameResult, Move, Player}
import com.degacth.montybank.{ClientJoined, GameFinished}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

class GameSpec extends TestKit(ActorSystem("test-system"))
  with AnyWordSpecLike
  with ImplicitSender
  with Matchers
  with BeforeAndAfterEach {

  import com.degacth.montybank.Utils.commonTimeout
  import com.degacth.montybank.messages
  import system.dispatcher

  var game: ActorRef = _
  val bankSize = 60
  val players = Map(
    "1" -> Player("1", "foo"),
    "2" -> Player("2", "bar"),
    "3" -> Player("3", "baz"),
  )
  val moves: Seq[Move] = (0 until 4) map (n => Move(n.toString, 10, 0, 0, ""))
  val playerAccountSum: Int = moves.map(_.price).sum

  override def beforeEach(): Unit = {
    super.beforeEach()
    game = system.actorOf(Game.props(new GameConfig {
      override val playerDb: Map[String, Player] = players
    }))

    def await(): Unit = Thread.sleep(100)

    val (_, playersRef) = Game.getBanksAndPlayersRef(game)

    players foreach { case (playerId, _) =>
      playersRef ! ClientJoined(playerId, system actorOf TestActors.blackholeProps)
    }
    await()

    players foreach { case (playerId, _) =>
      moves foreach { m =>
        system.actorSelection(playersRef.path / playerId) ! messages.Success(m)
      }
    }
    await()

    system.actorSelection(playersRef.path / "1") ! Deposit(10)
    system.actorSelection(playersRef.path / "2") ! Deposit(20)
    system.actorSelection(playersRef.path / "3") ! Deposit(30)
    await()
  }

  override def afterEach(): Unit = {
    super.afterEach()
    system stop game
  }

  "Game" must {
    "get game result" in {
      game ! GameFinished(bankSize)
      val result = receiveOne(commonTimeout.duration).asInstanceOf[List[GameResult]]
      val first :: _ :: last :: Nil = result

      first.player shouldBe "bar"
      last.player shouldBe "foo"
    }
  }
}
