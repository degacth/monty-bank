package com.degacth.montybank.routes

import akka.http.scaladsl.model.StatusCodes
import com.degacth.montybank.messages.{Accepted, Player}

class CommonRoutesSpec extends SpecBase {
  import com.degacth.montybank.messages.ConnectionMessagesOpts._

  "Common routes" must {
    "create player" in {
      Post("/players") ~> gameServer.routes ~> check {
        status shouldEqual StatusCodes.OK

        val body = responseAs[String]
        body.fromStr match {
          case Right(Accepted(_: Player)) =>
          case _ => throw new AssertionError(body)
        }
      }
    }

    "return player" in {
      Get(s"/players/${player.id}") ~> gameServer.routes ~> check {
        status shouldEqual StatusCodes.OK
        val body = responseAs[String]
      }
    }
  }
}
