package com.degacth.montybank.routes

import akka.actor.ActorRef
import akka.http.scaladsl.testkit.{ScalatestRouteTest, WSProbe}
import com.degacth.montybank.Server
import com.degacth.montybank.actors.{Game, GameConfig}
import com.degacth.montybank.messages.Player
import org.scalatest.BeforeAndAfterEach
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

trait SpecBase extends AnyWordSpecLike with ScalatestRouteTest with BeforeAndAfterEach with Matchers {
  val player: Player = Player("id", "test-name")
  var gameServer: Server = _
  var game: ActorRef = _

  override def beforeEach(): Unit = {
    game = system.actorOf(Game.props(new GameConfig {
      override val playerDb: Map[String, Player] = Map(
        player.id -> player
      )
    }))
    gameServer = new Server(game)
  }

  override def afterEach(): Unit = {
  }

  def withWsConnection(role: String, id: String)(body: WSProbe => Unit): Unit = {
    val wsClient = WSProbe()
    WS(s"/ws/$role/$id", wsClient.flow) ~> gameServer.routes ~> check {
      body(wsClient)
    }
  }
}
