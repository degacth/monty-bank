package com.degacth.montybank.routes

import com.degacth.montybank.Server
import com.degacth.montybank.messages._

class WsBankSpec extends SpecBase {

  import com.degacth.montybank.messages.ConnectionMessagesOpts._

  val bankId = "0"

  "Bank" must {
    "connect to server" in {
      withWsConnection(Server.bankRoleName, bankId) { _ =>
        isWebSocketUpgrade shouldBe true
      }
    }

    "start game" in {
      withWsConnection(Server.bankRoleName, bankId) { bankClient =>
        withWsConnection(Server.playerRoleName, player.id) { playerClient =>
          bankClient sendMessage StartGame.toStr
          playerClient.expectMessage().asTextMessage.getStrictText.fromStr shouldBe Right(StartGame)
          playerClient.expectMessage().asTextMessage.getStrictText.fromStr match {
            case Right(NextMove(_)) =>
            case other => throw new AssertionError(other.toString)
          }
        }
      }
    }
  }
}
