package com.degacth.montybank.routes

import akka.testkit.TestProbe
import com.degacth.montybank.Server
import com.degacth.montybank.messages._

class WsPlayerSpec extends SpecBase {
  import ConnectionMessagesOpts._

  "Player" must {
    "connect to server" in {
      withWsConnection(Server.playerRoleName, player.id) { _ =>
        isWebSocketUpgrade shouldBe true
      }
    }

    "set success move result" in {
      val probe = TestProbe()
      // TODO check result
    }
  }
}
