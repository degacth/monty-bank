package com.degacth.montybank.e2e.utils

import com.degacth.montybank.utils.PriceFormat
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.{ExpectedConditions, WebDriverWait}

class PlayerPO(driver: WebDriver) extends PageObject(driver) {
  override def getPath: String = "/"

  def username: String = byCSS("header .username").getText
  def isGameActive: Boolean = byCSS(".game-active").isDisplayed
  def selectDoor(n: Int): Unit = byCSSAll(".options .door").get(n).click()
  def hasOpened: Boolean = byCSS(".opened").isDisplayed
  def hasSuccess: Boolean = byCSS(".move-result-message").getText.contains("WIN")
  def hasLose: Boolean = byCSS(".move-result-message").getText.contains("LOSE")
  def waitAccountPrice(n: Int): Boolean = driverWait.until(
    ExpectedConditions.textToBePresentInElement(byCSS(".player-account"), PriceFormat.toPrice(n))
  )

  def deposit(n: Int): Unit = {
    (0 until n) foreach (_ => byCSS(".account-manager .increase").click())
  }

  def submitDeposit(): Unit = {
    byCSS(".account-manager .submit-deposit").click()
  }
}
