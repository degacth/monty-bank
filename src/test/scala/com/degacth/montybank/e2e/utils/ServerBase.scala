package com.degacth.montybank.e2e.utils

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.degacth.montybank.actors.Game
import com.degacth.montybank.{AppConf, Server}

trait ServerBase {
  lazy val server: Server = ServerBase.server
  lazy val system: ActorSystem = ServerBase.system
}

object ServerBase {
  private lazy val system: ActorSystem = ActorSystem("test-app")

  private lazy val server = {
    implicit val (s, ec) = (system, system.dispatcher)
    val (host, port) = AppConf.getHostPort

    val server = new Server(system.actorOf(Game.props()))
    val binding = Http().bindAndHandle(server.routes, host, port)
    logServerEvents(s"Server started on $host:$port")

    Runtime.getRuntime.addShutdownHook(new Thread(() => {
      logServerEvents("System is shutting down")
      binding flatMap (_.unbind()) onComplete { _ =>
        system.terminate()
      }
    }))

    server
  }

  private def logServerEvents(event: String): Unit = system.log info s"------------ $event ------------"
}