package com.degacth.montybank.e2e.utils

import com.degacth.montybank.utils.PriceFormat
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.ExpectedConditions

class BankPO(driver: WebDriver) extends PageObject(driver) {
  override def getPath: String = "/bank"

  def runGame(): Unit = {
    byCSS(".run-button").click()
  }

  def waitPrice(price: Int) = driverWait.until(
    ExpectedConditions.textToBePresentInElement(byCSS(".bank-size"), PriceFormat.toPrice(price))
  )
}
