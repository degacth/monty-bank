package com.degacth.montybank.e2e.utils

import java.util

import com.degacth.montybank.AppConf
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.{By, WebDriver, WebElement}

abstract class PageObject(driver: WebDriver) {
  val driverWait = new WebDriverWait(driver, 10)

  def getPath: String

  def init(): Unit = driver.get(s"$baseUrl$getPath")
  def stop(): Unit = driver.quit()
  def refresh(): Unit = driver.navigate().refresh()

  protected def byCSS(selector: String): WebElement = driver findElement By.cssSelector(selector)
  protected def byCSSAll(selector: String): util.List[WebElement] = driver findElements By.cssSelector(selector)

  private val baseUrl: String = {
    val (host, port) = AppConf.getHostPort
    s"http://$host:$port"
  }
}
