package com.degacth.montybank.e2e.utils

import java.util.concurrent.TimeUnit

import akka.actor.PoisonPill
import org.openqa.selenium.WebDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import scala.collection.parallel.CollectionConverters._
import scala.concurrent.{Await, Future}
import concurrent.ExecutionContext.Implicits.global
import concurrent.duration._

trait TestInitializer extends ServerBase
  with AnyWordSpecLike
  with Matchers
  with BeforeAndAfterAll {

  TestInitializer.initProp

  override def beforeAll(): Unit = {
    super.beforeAll()
    server
  }

  override def afterAll(): Unit = {
    super.afterAll()
    system.actorSelection("/user/*") ! PoisonPill
  }

  def withBankAndPlayers(playersCount: Int)(f: (BankPO, Vector[PlayerPO]) => Unit): Unit = {
    val bankFuture = Future {
      new BankPO(createDriver)
    }
    val players = (0 until playersCount).par map (_ => new PlayerPO(createDriver))
    val bank = Await.result(bankFuture, 10.seconds)
    val pageObjects: List[PageObject] = bank +: players.toList

    pageObjects foreach (_.init())
    try {
      f(bank, players.toVector)
    } finally pageObjects foreach (_.stop())
  }

  def withPlayer(f: PlayerPO => Unit): Unit = {
    val player = createPlayer
    player.init()

    try f(player) finally player.stop()
  }

  private def createDriver: WebDriver = {
    val driver = new FirefoxDriver()
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS)
    driver
  }

  private def createPlayer = new PlayerPO(createDriver)
}

object TestInitializer {
  private lazy val initProp: String = {
    System.setProperty("webdriver.gecko.driver", "node_modules/geckodriver/geckodriver")
  }
}
