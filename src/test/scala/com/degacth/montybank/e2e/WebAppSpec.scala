package com.degacth.montybank.e2e

import com.degacth.montybank.FitTag
import com.degacth.montybank.e2e.utils.TestInitializer
import scala.collection.parallel.CollectionConverters._

class WebAppSpec extends TestInitializer {
  "Web client" must {
    val wrongDoor = 0
    val successDoor = 1

    "show username" taggedAs FitTag in {
      withPlayer { player =>
        val username = player.username
        username.isEmpty === false
        player.refresh()
        username === player.username
      }
    }

    "start game" in {
      val playersCount = 2
      val toDeposit = 6
      val account = 10

      withBankAndPlayers(playersCount) { (bank, players) =>
        bank.runGame()
        players.par foreach { p =>
          p.isGameActive === true
          p.selectDoor(wrongDoor)
          p.hasOpened

          p.selectDoor(successDoor)
          p.hasSuccess
          p.waitAccountPrice(account) shouldBe true
          p.deposit(toDeposit)
          p.submitDeposit()
          p.waitAccountPrice(account - toDeposit)
        }

        bank.waitPrice(toDeposit * playersCount)
      }
    }

    "show lose message" in {
      withBankAndPlayers(1) { (bank, players) =>
        bank.runGame()

        val player = players.head
        player.selectDoor(wrongDoor)
        player.hasOpened
        player.selectDoor(wrongDoor)
        player.hasLose
      }
    }
  }
}
