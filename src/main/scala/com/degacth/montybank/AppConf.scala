package com.degacth.montybank

import com.typesafe.config.ConfigFactory

object AppConf {
  private val appConf = ConfigFactory.load("application").getConfig("app")

  lazy val getHostPort: (String, Int) = {
    val serverParams = appConf.getConfig("server")
    (serverParams.getString("host"), serverParams.getInt("port"))
  }

  lazy val moveTimeout: Int = appConf.getConfig("game.move").getInt("timeout")
  lazy val indexType: String = appConf.getConfig("game.move").getString("indexType")
  lazy val startTimeout: Int = appConf.getConfig("game").getInt("startTimeout")
  lazy val movePrice: Int = appConf.getConfig("game.move").getInt("price")
  lazy val maxMoves: Int = appConf.getConfig("game").getInt("maxMoves")
}
