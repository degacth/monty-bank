package com.degacth.montybank

import akka.util.Timeout
import concurrent.duration._
import scala.language.implicitConversions

object Utils {
  implicit class TemplateFormatter(string: String) {
    def templateWith(replacement: Map[String, Any]): String = (string /: replacement) { (res, entry) =>
      res.replaceAll("#\\{%s\\}".format(entry._1), entry._2.toString)
    }
  }

  implicit val commonTimeout: Timeout = Timeout(10.seconds)
}
