package com.degacth.montybank

import akka.actor.Status.Success
import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.ws.{Message, TextMessage}
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, StatusCodes}
import akka.http.scaladsl.server.{Directives, Route}
import akka.pattern._
import akka.stream.scaladsl.{Flow, GraphDSL, Merge, Sink, Source}
import akka.stream.{AbruptStageTerminationException, CompletionStrategy, FlowShape, OverflowStrategy}
import com.degacth.montybank.actors.Game
import com.degacth.montybank.messages._

class Server(game: ActorRef)(implicit as: ActorSystem) extends Directives {

  import Server._
  import Utils._
  import as.dispatcher

  private lazy val roleTemplate = scala.io.Source.fromResource("role-template.html").getLines().mkString("\n")
  private lazy val playerTemplate = makeTemplateForRole(playerRoleName)
  private lazy val bankTemplate = makeTemplateForRole(bankRoleName)

  val (banks: ActorRef, players: ActorRef) = Game.getBanksAndPlayersRef(game)

  val routes: Route = concat(
    pathEndOrSingleSlash {
      get {
        complete(playerTemplate)
      }
    },

    pathPrefix(bankPath) {
      get {
        complete(bankTemplate)
      }
    },

    pathPrefix(wsClientPath / Segment / Segment) {
      case (`bankRoleName`, _: String) => handleWebSocketMessages(makeConnectionHandler(banksSource, banks, "0"))
      case (`playerRoleName`, id: String) => handleWebSocketMessages(makeConnectionHandler(playersSource, players, id))
      case (_, _) => complete(StatusCodes.NotFound)
    },

    pathPrefix("js" / Segment) { role: String => getFromDirectory(s"web-client-$role/target/scala-2.13") },

    pathPrefix("node_modules") {
      getFromDirectory("node_modules")
    },

    pathPrefix("players") {
      import com.degacth.montybank.messages.ConnectionMessagesOpts._

      concat(
        post {
          onSuccess(players ? CreatePlayer) {
            case p: Player => complete(StatusCodes.OK, Accepted(p).toStr)
          }
        },

        get {
          pathPrefix(Segment) { id: String =>
            onSuccess(players ? GetPlayer(id)) {
              case Some(p: Player) => complete(StatusCodes.OK, Accepted(p).toStr)
              case _ => complete(StatusCodes.NotFound)
            }
          }
        }
      )
    }
  )

  private val playersSource = connectionSource
  private val banksSource = connectionSource

  private def makeConnectionHandler(source: Source[ConnectionMessage, ActorRef],
                                    connectionsHolder: ActorRef,
                                    id: String): Flow[Message, Message, Any] =

    Flow fromGraph GraphDSL.create(source) { implicit b =>
      connection =>
        import GraphDSL.Implicits._
        import com.degacth.montybank.messages.ConnectionMessagesOpts._

        val mat = b.materializedValue map (ClientJoined(id, _))
        val merge = b add Merge[Any](2)
        val inMsg: FlowShape[Message, ConnectionMessage] = b.add(Flow[Message] collect {
          case TextMessage.Strict(msg) => msg.fromStr.getOrElse(ParseErrorMessage(msg))
        })
        val outMsg: FlowShape[ConnectionMessage, TextMessage] = b.add(Flow[Any] collect {
          case msg: ConnectionMessage => TextMessage(msg.toStr)
        })

        val toClientFlow = b.add(Flow[Any] collect {
          case m: ConnectionMessage => ToHost(id, m)
        })

        val sink = Sink.actorRef[Any](connectionsHolder, ClientLeave(id), {
          case _: AbruptStageTerminationException =>
          case e: Throwable => as.log.error(e, "Connection Sink error")
        }: PartialFunction[Throwable, Any])

        mat ~> merge ~> sink
        inMsg ~> toClientFlow ~> merge
        connection ~> outMsg

        FlowShape(inMsg.in, outMsg.out)
    }

  private def connectionSource: Source[ConnectionMessage, ActorRef] = Source.actorRef[ConnectionMessage](
    {
      case _: Success => CompletionStrategy.immediately
    }: PartialFunction[Any, CompletionStrategy], {
      case e: Throwable => new Exception("Connection source exception")
    }: PartialFunction[Any, Throwable],
    5, OverflowStrategy.fail
  )

  private def makeTemplateForRole(role: String) = HttpEntity(
    ContentTypes.`text/html(UTF-8)`, roleTemplate templateWith Map("role" -> role)
  )
}

object Server {
  val bankPath = "bank"
  val wsClientPath = "ws"
  val playerRoleName = "player"
  val bankRoleName = "bank"
}
