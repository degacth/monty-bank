package com.degacth.montybank

import akka.actor.ActorRef
import com.degacth.montybank.messages.ConnectionMessage

case class ToHost(playerId: String, msg: ConnectionMessage)
case class ClientJoined(clientId: String, ref: ActorRef)
case class ClientLeave(clientId: String)
case class GetPlayer(playerId: String)
case class GameFinished(sum: Int)

case object CreatePlayer
case object GetGameResults
