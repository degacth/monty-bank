package com.degacth.montybank.actors

import java.util.UUID

import akka.actor._
import akka.pattern._
import com.degacth.montybank.{AppConf, ClientJoined, GameFinished, ToHost}
import com.degacth.montybank.messages._
import concurrent.duration._

class BanksActor extends Actor with ActorLogging {

  import context.dispatcher
  import com.degacth.montybank.Utils.commonTimeout

  var startCancel: Option[Cancellable] = None
  var clients: List[ActorRef] = List.empty
  var movesCounter: Int = _
  var depositSum: Int = _

  override def receive: Receive = {
    case ClientJoined(_, ref) => clients = ref :: clients

    case ToHost(_, msg) => msg match {
      case StartGame =>
        context.parent ! StartGame
        movesCounter = AppConf.maxMoves

        startCancel = Some(
          context.system.scheduler
            .scheduleWithFixedDelay(AppConf.startTimeout.millis, AppConf.moveTimeout.millis) { () =>
              movesCounter match {
                case n if n <= 0 =>
                  context.parent ? GameFinished(depositSum) foreach {
                    case results: List[GameResult] => sendForAll(GameResults(results))
                  }
                  startCancel foreach (_.cancel())
                  startCancel = None

                case _ =>
                  context.parent ! Move(
                    UUID.randomUUID().toString,
                    AppConf.movePrice,
                    System.currentTimeMillis(),
                    AppConf.moveTimeout, AppConf.indexType
                  )

                  movesCounter -= 1
              }
            })

      case m => log.warning("Unsupported message to bank host: {}", m)
    }

    case deposit: Deposit =>
      depositSum += deposit.payload
      sendForAll(deposit)
  }

  private def sendForAll(msg: Any): Unit = clients foreach (_ ! msg)
}

object BanksActor {
  def props(): Props = Props[BanksActor]
}
