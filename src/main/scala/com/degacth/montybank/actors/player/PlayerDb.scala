package com.degacth.montybank.actors.player

import java.util.UUID

import akka.actor._
import akka.util.Timeout

import concurrent.duration._
import com.degacth.montybank.{CreatePlayer, GetPlayer}
import com.degacth.montybank.messages.{GameResult, Player}
import com.github.javafaker.Faker

class PlayerDb(var players: Map[String, Player]) extends Actor {
  private val faker = new Faker()

  override def receive: Receive = {
    case CreatePlayer =>
      val p = createFakePlayer()
      players += p.id -> p
      sender() ! p

    case GetPlayer(playerId) => sender() ! players.get(playerId)
    case results: List[GameResult] => sender() ! (results collect {
      case result: GameResult if players.get(result.player).isDefined =>
        result.copy(player = players(result.player).name)
    })
  }

  private def createFakePlayer() = Player(
    UUID.randomUUID().toString, faker.name().fullName()
  )
}

object PlayerDb {
  val timeout: Timeout = Timeout(10.seconds)
  def props(players: Map[String, Player]): Props = Props(classOf[PlayerDb], players)
}
