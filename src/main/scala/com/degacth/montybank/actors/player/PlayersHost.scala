package com.degacth.montybank.actors.player

import akka.actor._
import akka.pattern._
import com.degacth.montybank.{ClientJoined, ClientLeave, CreatePlayer, GetGameResults, GetPlayer, ToHost}
import com.degacth.montybank.messages._

import scala.concurrent.Future

class PlayersHost(playersDb: ActorRef) extends Actor with ActorLogging {

  import com.degacth.montybank.Utils.commonTimeout
  import context.dispatcher

  private var moves: Map[String, Move] = _

  override def receive: Receive = {
    case ClientJoined(playerId, ref) => context.actorOf(PlayerActor.props(ref, playerId), name = playerId)
    case ClientLeave(playerId) => context.child(playerId) foreach context.stop
    case StartGame =>
      moves = Map.empty
      sendForAll(StartGame)

    case CreatePlayer => playersDb forward CreatePlayer
    case m: GetPlayer => playersDb forward m

    case ToHost(playerId, Success(m: Move)) => moves.get(m.id) match {
      case Some(move) => sendForPlayer(playerId, Success(move))
      case None => log.warning("No such move {} as success move", m)
    }

    case ToHost(playerId, msg) => sendForPlayer(playerId, msg)

    case move: Move =>
      moves += move.id -> move
      sendForAll(move)

    case Accepted(deposit: Deposit) => context.parent ! deposit

    case GetGameResults =>
      val sndr = sender()
      val playersResults = context.children.map { child =>
        child ? GetGameResults
      }

      Future.sequence(playersResults) flatMap (result => playersDb ? result) foreach {
        sndr ! _
      }
  }

  private def sendForPlayer(playerId: String, msg: ConnectionMessage): Unit = context.child(playerId) match {
    case Some(player) => player ! msg
    case None => log.warning("Message for NONE player ({}): {}", playerId, msg)
  }

  private def sendForAll(msg: Any): Unit = context.children foreach (_ ! msg)
}

object PlayersHost {
  def props(playerDb: ActorRef): Props = Props(classOf[PlayersHost], playerDb)
}

class PlayerActor(connection: ActorRef, player: String) extends Actor with ActorLogging {
  private var playerState: PlayerState = PlayerState()

  override def receive: Receive = {
    case StartGame =>
      playerState = PlayerState()
      connection ! StartGame

    case m: Move => connection ! NextMove(m)

    case Success(move: Move) =>
      playerState = playerState.copy(account = playerState.account + move.price)
      connection ! Accepted(playerState)

    case d@Deposit(n) if playerState.canDeposit(n) =>
      playerState = playerState.copy(
        deposit = playerState.deposit + n,
        account = playerState.account - n,
      )
      context.parent ! Accepted(d)
      connection ! Accepted(playerState)

    case Deposit(_) => connection ! ClientErrorMessage(DepositExceeded)
    case GetGameResults => sender() ! GameResult(player, playerState.account, playerState.deposit)
  }
}

object PlayerActor {
  def props(ref: ActorRef, playerId: String): Props = Props(classOf[PlayerActor], ref, playerId)
}
