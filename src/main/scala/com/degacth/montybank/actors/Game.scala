package com.degacth.montybank.actors

import akka.actor.{Actor, ActorLogging, ActorNotFound, ActorPath, ActorRef, ActorSystem, Props}
import akka.pattern._
import akka.util.Timeout
import com.degacth.montybank.{GameFinished, GetGameResults}
import com.degacth.montybank.actors.player.{PlayerDb, PlayersHost}
import com.degacth.montybank.messages.{Deposit, GameResult, Move, Player, StartGame}
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Random, Success}

class Game(config: GameConfig) extends Actor with ActorLogging {

  import context.dispatcher
  import com.degacth.montybank.Utils.commonTimeout

  val playersDb: ActorRef = context.actorOf(PlayerDb.props(config.playerDb), name = Game.playersDbPath)
  val players: ActorRef = context.actorOf(PlayersHost.props(playersDb), name = Game.playersPath)
  val banks: ActorRef = context.actorOf(BanksActor.props(), name = Game.banksPath)

  override def receive: Receive = {
    case StartGame => players ! StartGame
    case m: Move => players ! m
    case deposit: Deposit => banks ! deposit
    case GameFinished(bank: Int) =>
      val sndr = sender()
      players ? GetGameResults onComplete {
        case Success(results: List[GameResult]) =>
          results.length match {
            case n if n <= 1 => sndr ! results
            case n =>
              val lessByDeposits = results.sortBy(_.deposit).takeWhile(_.deposit == results.head.deposit)
              val loser = Random.shuffle(lessByDeposits).head // TODO error head of empty list

              val bankChunk: Int = bank / (n - 1)
              sndr ! results.collect {
                case result if result.player == loser.player => result
                case result => result.copy(account = result.account + bankChunk)
              }.sortBy(_.account).reverse
          }

        case e => log.warning("Unsupported player result: {}", e)
      }
  }
}

object Game {
  val banksPath = "banks"
  val playersPath = "players"
  val playersDbPath = "players-db"

  def props(config: GameConfig = DefaultGameConfig): Props = Props(classOf[Game], config)

  def getBanksAndPlayersRef(game: ActorRef)
                           (implicit as: ActorSystem,
                            timeout: Timeout,
                            ec: ExecutionContext): (ActorRef, ActorRef) =
    Await.result(getConnectionsHostActor(game.path / banksPath), timeout.duration) ->
      Await.result(getConnectionsHostActor(game.path / playersPath), timeout.duration)

  private def getConnectionsHostActor(path: ActorPath, retryN: Int = 10)
                                     (implicit as: ActorSystem, timeout: Timeout, ec: ExecutionContext): Future[ActorRef] = {

    as.actorSelection(path).resolveOne() recoverWith {
      case _: ActorNotFound =>
        if (retryN > 0) {
          Thread.sleep(50)
          getConnectionsHostActor(path, retryN - 1)
        } else throw new RuntimeException(s"Can't find actor with path $path")
    }
  }
}

trait GameConfig {
  val playerDb: Map[String, Player] = Map.empty
}

object DefaultGameConfig extends GameConfig
