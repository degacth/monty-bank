package com.degacth.montybank

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.degacth.montybank.actors.Game

import scala.concurrent.ExecutionContextExecutor
import scala.io.StdIn

object Main extends App {
  implicit val system: ActorSystem = ActorSystem("app")
  implicit val ec: ExecutionContextExecutor = system.dispatcher

  val server = new Server(system.actorOf(Game.props()))
  val (host, port) = AppConf.getHostPort
  val binding = Http().bindAndHandle(server.routes, host, port)

  StdIn.readLine("Press enter to exit")
  binding flatMap (_.unbind()) onComplete { _ =>
    system.terminate()
  }
}
