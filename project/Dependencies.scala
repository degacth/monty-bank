import sbt._

object Dependencies {

  lazy val scalaTestVer = "3.1.0"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalaTestVer

  lazy val akkaVer = "2.6.1"
  lazy val akkaHttpVer = "10.1.11"
  lazy val akkaDeps = Seq(
    "com.typesafe.akka" %% "akka-stream" % akkaVer,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVer % Test,
    "com.typesafe.akka" %% "akka-persistence" % akkaVer,
    "com.typesafe.akka" %% "akka-persistence-query" % akkaVer,
  )
  lazy val akkaHttpDeps = Seq(
    "com.typesafe.akka" %% "akka-http" % akkaHttpVer,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVer % Test
  )

  lazy val faker = "com.github.javafaker" % "javafaker" % "1.0.1"

  lazy val seleniumVer = "3.141.59"
  lazy val selenium = Seq(
    "org.seleniumhq.selenium" % "selenium-java" % seleniumVer,
  )
}
