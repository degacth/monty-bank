package com.degacth.montybank.webui.app

import com.degacth.montybank.messages.{Deposit, GameResult, GameResults}
import com.degacth.montybank.utils.PriceFormat
import com.degacth.montybank.webui.app.bank.{BankState, BankStateManager, BankVueAppFactory}

import scala.concurrent.Future
import scala.scalajs.js._

class AppComponentTest extends BaseSpec {
  var bank: BankPO = _
  var sm: BankStateManager = _

  override def componentLiterals: (Object, Object) = (BankVueAppFactory, Dynamic.literal(sm = sm.asInstanceOf[Any]))

  override def beforeEach(): Unit = {
    sm = new BankStateManager {
      override val state: BankState = new BankState {}
      override def startGame(): Unit = state.gameStarted = true
    }
    super.beforeEach()
    bank = new BankPO(this)
  }

  "Bank app" must {
    "start game" in {
      bank.run()
      assert(sm.state.gameStarted === true)
    }

    "increase bank size" in {
      val deposit = 10
      sm.onDeposit(Deposit(deposit))
      Future.unit map { _ =>
        assert(bank.isBankSizeEq(deposit))
      }
    }

    "show results" in {
      sm.onGameResults(GameResults(GameResult("foo", 40, 30) :: GameResult("foo", 40, 30) :: Nil))
      Future.unit map { _ =>
        assert(bank.resultsCount === 2)
      }
    }

    "rise in size bank" in {
      assert(bank.bankSizeWidth > 0)
    }
  }
}

class BankPO(spec: BaseSpec) {
  private val bankSizeSelector = ".bank-size"
  def run(): Unit = spec.byCss(".run-button").click()
  def isBankSizeEq(size: Int): Boolean = spec.byCss(bankSizeSelector).textContent == PriceFormat.toPrice(size)
  def bankSizeWidth: Int = spec.byCss(bankSizeSelector).style.width.replace("px", "").toInt
  def resultsCount: Int = spec.byCssAll(".game-results tbody tr").length
}
