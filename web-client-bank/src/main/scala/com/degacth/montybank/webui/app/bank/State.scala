package com.degacth.montybank.webui.app.bank

import com.degacth.mantybank.webui.state.{State, StateManager, WsManager}
import com.degacth.montybank.messages._
import org.scalajs.dom

trait BankState extends State {
  var gameStarted = false
  var bankSize = 0
  var gameResults: Option[GameResults] = None
}

trait BankStateManager extends StateManager {
  override val state: BankState
  def startGame()
  def onGameResults(results: GameResults): Unit = state.gameResults = Some(results)
  def onDeposit(deposit: Deposit): Unit = state.bankSize += deposit.payload
}

class WsBankStateManager(override val state: BankState) extends BankStateManager with WsManager {
  override val url: String = s"ws://${dom.window.location.host}/ws/bank/0"

  override def onMessage(msg: ConnectionMessage): Unit = msg match {
    case StartGame => state.gameStarted = true
    case Deposit(n) => state.bankSize += n
    case results: GameResults => onGameResults(results)
    case _ => ???
  }

  override def startGame(): Unit = send(StartGame)

  init()
}
