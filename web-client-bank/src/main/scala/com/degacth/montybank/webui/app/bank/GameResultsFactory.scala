package com.degacth.montybank.webui.app.bank

import com.degacth.mantybank.webui.vue.BaseComponent
import com.degacth.montybank.messages.{GameResult, GameResults}

import scalajs.js._
import JSConverters._

abstract class GameResultsComponent extends BaseComponent {
  var results: GameResults = _
}

object GameResultsFactory extends GameResultsComponent {
  type C = GameResultsComponent

  override val template: String =
    <div>
      <table class="uk-table game-results">
        <caption>Game Resutls</caption>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Account</th>
            <th>Deposit</th>
          </tr>
        </thead>

        <tbody>
          <tr v-for="(result, index) in gameResults">
            <td>[[ index ]]</td>
            <td>[[ name(result) ]]</td>
            <td>[[ account(result) ]]</td>
            <td>[[ deposit(result) ]]</td>
          </tr>
        </tbody>
      </table>
    </div> toString

  override val props: Array[String] = Array("results")

  override val methods: Dynamic = Dynamic.literal(
    name = (result: GameResult) => result.player,
    account = (result: GameResult) => result.account,
    deposit = (result: GameResult) => result.deposit,
  )

  override val computed: Dynamic = Dynamic.literal(
    gameResults = { self: C => self.results.payload.toJSArray }: ThisFunction,
  )
}
