package com.degacth.montybank.webui.app

import com.degacth.mantybank.webui.WebApp
import com.degacth.mantybank.webui.vue.Vue
import com.degacth.montybank.webui.app.bank.{BankState, BankVueAppFactory, WsBankStateManager}
import org.scalajs.dom

import scala.scalajs.js._

object WebUI extends WebApp {
  def init(container: dom.Element): Unit = {
    val sm = new WsBankStateManager(new BankState {})
    val app = Vue extend BankVueAppFactory
    Dynamic.newInstance(app.asInstanceOf[Dynamic])(Dynamic.literal(el = container, propsData = Dynamic.literal(
      sm = sm.asInstanceOf[Any])
    ))
  }
}
