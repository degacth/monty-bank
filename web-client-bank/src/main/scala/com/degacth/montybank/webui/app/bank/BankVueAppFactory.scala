package com.degacth.montybank.webui.app.bank

import com.degacth.mantybank.webui.vue.BaseComponent
import scalajs.js._

abstract class VueApp extends BaseComponent {
  var sm: BankStateManager = _
  val startGame: ThisFunction
}

object BankVueAppFactory extends VueApp {
  type C = VueApp
  val minBankWidth = 32

  val template: String =
    <div class="uk-container application">
      <app-game-results v-if="hasResults" v-bind:results="results"></app-game-results>
      <a class="bank-size run-button"
         v-else=""
         v-bind:style="bankSizeStyle"
         v-on:click="startGame">[[ size | toPrice ]]</a>
    </div> toString

  val components: Dynamic = Dynamic.literal(
    "app-game-results" -> GameResultsFactory,
  )

  override val startGame: ThisFunction = (self: C) => {
    self.sm.startGame()
  }

  override val methods: Dynamic = Dynamic.literal(
    startGame = startGame
  )

  override val props: Array[String] = Array("sm")
  override val computed: Dynamic = Dynamic.literal(
    size = { self: C => self.sm.state.bankSize }: ThisFunction,
    hasResults = { self: C => self.sm.state.gameResults.isDefined }: ThisFunction,
    results = { self: C => self.sm.state.gameResults.get }: ThisFunction,
    bankSizeStyle = { self: C => {
      val size = Math.max(self.sm.state.bankSize, minBankWidth)
      Dynamic.literal(
        width = s"${size}px",
        height = s"${size}px",
      )
    }
    }: ThisFunction
  )
}
