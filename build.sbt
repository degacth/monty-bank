import Dependencies._
import sbt.Keys.libraryDependencies

import scala.language.postfixOps
import sbtcrossproject.CrossPlugin.autoImport.{CrossType, crossProject}

ThisBuild / scalaVersion := "2.13.1"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.degacth"
ThisBuild / organizationName := "degacth"
Global / onChangedBuildSource := ReloadOnSourceChanges

lazy val root = (project in file("."))
  .dependsOn(sharedJVM)
  .settings(
    name := "monty-bank",
    fork in Test := true,
    libraryDependencies ++= akkaDeps,
    libraryDependencies ++= akkaHttpDeps,
    libraryDependencies ++= Seq(faker, scalaTest % Test),
    libraryDependencies ++= selenium.map(_ % Test),
    libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "0.2.0",
    libraryDependencies += "com.github.dnvriend" %% "akka-persistence-inmemory" % "2.5.15.2",
  )

lazy val circeVersion = "0.12.3"

lazy val circe = Seq(
  "io.circe" -> "circe-core",
  "io.circe" -> "circe-generic",
  "io.circe" -> "circe-parser",
)

def createCirceDeps[T](f: (String, String) => T): Seq[T] =
  circe map { case (org: String, art: String) => f(org, art) }

lazy val shared = crossProject(JSPlatform, JVMPlatform)
  .crossType(CrossType.Pure)
  .jvmSettings(
    libraryDependencies ++= createCirceDeps(_ %% _) map (_ % circeVersion)
  )
  .jsSettings(
    libraryDependencies ++= createCirceDeps(_ %%% _) map (_ % circeVersion)
  )

lazy val scalaJSDomVer = "0.9.7"
lazy val jsScalacOptions = Seq(
  "-language:postfixOps",
  "-P:scalajs:sjsDefinedByDefault",
)

lazy val commonWebClientSettings = Seq(
  scalacOptions ++= jsScalacOptions,
  scalaJSUseMainModuleInitializer := true,
  name := "web-ui",
  jsEnv := new org.scalajs.jsenv.jsdomnodejs.JSDOMNodeJSEnv(),
  skip in packageJSDependencies := false,

  jsDependencies ++= Seq(
    ProvidedJS / "stubs.js" dependsOn "vue.js",
    ProvidedJS / "node_modules/lodash/lodash.min.js",
    ProvidedJS / "node_modules/uikit/dist/js/uikit.min.js" dependsOn "stubs.js",
    ProvidedJS / "node_modules/uikit/dist/js/uikit-icons.min.js",
    ProvidedJS / "node_modules/vue/dist/vue.js",
    ProvidedJS / "node_modules/sinon/pkg/sinon.js" % Test,
  ),

  libraryDependencies ++= Seq(
    "org.scala-lang.modules" %%% "scala-xml" % "1.2.0",
    "org.scalatest" %%% "scalatest" % scalaTestVer % Test,
    "org.scala-js" %%% "scalajs-dom" % scalaJSDomVer,
  ),
)

lazy val webClientPlayer = (project in file("web-client-player"))
  .enablePlugins(ScalaJSPlugin, JSDependenciesPlugin)
  .dependsOn(sharedJS, webClient)
  .settings(commonWebClientSettings)
  .settings(name := "player")

lazy val webClientBank = (project in file("web-client-bank"))
  .enablePlugins(ScalaJSPlugin, JSDependenciesPlugin)
  .dependsOn(sharedJS, webClient)
  .settings(commonWebClientSettings)
  .settings(name := "bank")

lazy val webClient = (project in file("web-client"))
  .dependsOn(sharedJS)
  .enablePlugins(ScalaJSPlugin, JSDependenciesPlugin)
  .settings(commonWebClientSettings)
  .settings(
    scalaJSUseMainModuleInitializer := false,
  )

lazy val sharedJVM = shared.jvm
lazy val sharedJS = shared.js
