package com.degacth.mantybank.webui.vue

import org.scalajs.dom.raw.{HTMLElement, NodeListOf}

import scala.concurrent.{ExecutionContext, Future}
import scala.scalajs.js._

trait ComponentSpec {
  var component: Vue = _

  def componentLiterals: (Object, Object)

  protected def prepare(): Unit = {
    val (componentData, propsData) = componentLiterals

    component = Dynamic.newInstance(Vue.extend(componentData).asInstanceOf[Dynamic])(Dynamic.literal(
      propsData = propsData
    )).asInstanceOf[Vue].$mount()
  }

  def byCss(selector: String): HTMLElement = component.$el.querySelector(selector).asInstanceOf[HTMLElement]

  def byCssAll(selector: String): NodeListOf[HTMLElement] =
    component.$el.querySelectorAll(selector).asInstanceOf[NodeListOf[HTMLElement]]

  def inFuture[T](body: => T)(implicit ec: ExecutionContext): Future[T] = Future.unit map { _ => body }
}
