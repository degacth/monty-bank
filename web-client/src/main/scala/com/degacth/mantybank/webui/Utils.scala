package com.degacth.mantybank.webui

import org.scalajs.dom

import scala.scalajs.js

object Utils {
  def dir(n: Any*): Unit = {
    val Seq(head, others@_*) = n.map(_.asInstanceOf[js.Any])
    dom.window.console.dir(head, others: _*)
  }
}
