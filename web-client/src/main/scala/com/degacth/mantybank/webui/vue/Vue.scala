package com.degacth.mantybank.webui.vue

import com.degacth.montybank.utils.PriceFormat
import org.scalajs.dom

import scala.scalajs.js.annotation.JSGlobal
import scalajs.js._
import scalajs.js

@native
@JSGlobal
class Vue(opts: Dynamic) extends Object {
  def $mount(): Vue = native
  val $data: js.Dynamic = js.native

  val $el: dom.Element = js.native
  val $options: js.Dynamic = js.native
  val $parent: Vue = js.native
  val $root: Vue = js.native
  val $refs: js.Dictionary[js.Any] = js.native
  val $slots: js.Dictionary[js.Any] = js.native
  val $scopedSlots: js.Dynamic = js.native
  val $isServer: Boolean = js.native
  val $ssrContext: js.Any = js.native
  val $props: js.Any = js.native
  val $attrs: js.Dictionary[String] = js.native
  val $listeners: js.Dictionary[js.Any] = js.native

  def $mount(elementOrSelector: js.Any): Vue = js.native
  def $forceUpdate(): Unit = js.native
  def $destroy(): Unit = js.native
  def $set: js.Function = js.native
  def $delete: js.Function = js.native
  def $on(event: String, callback: js.Function): Vue = js.native
  def $on(event: js.Array[String], callback: js.Function): Vue = js.native
  def $once(event: String, callback: js.Function): Vue = js.native
  def $once(event: js.Array[String], callback: js.Function): Vue = js.native
  def $off(event: String, callback: js.Function): Vue = js.native
  def $off(event: js.Array[String], callback: js.Function): Vue = js.native
  def $emit(event: String, args: String*): Vue = js.native
  def $nextTick(callback: js.ThisFunction): Unit = js.native
  def $nextTick(): js.Promise[Unit] = js.native
}

@native
@JSGlobal
object Vue extends Object {
  def component(id: String, opts: Any): Unit = native
  def extend(opts: Object): Object = native
}

abstract class BaseComponent extends Object {
  val template: String
  val props: Array[String] = Array()
  val delimiters: Array[String] = Array("[[", "]]")
  val computed: Dynamic = Dynamic.literal()
  val methods: Dynamic = Dynamic.literal()
  def data(): Dynamic = Dynamic.literal()
  val filters: Dynamic = BaseComponent.filters
  val watch: Dynamic = Dynamic.literal()
}

object BaseComponent {
  val filters: Dynamic = Dynamic.literal(
    toPrice = (n: Int) => PriceFormat.toPrice(n)
  )
}
