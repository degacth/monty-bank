package com.degacth.mantybank.webui.sinon

import scala.scalajs.js.annotation.JSGlobal
import scala.scalajs.js._

@native
@JSGlobal("sinon")
object Sinon extends Object {
  def fake(args: Any*): Sinon = native
  val fake: Sinon = native
}

@native
@JSGlobal
class Sinon extends Object {
  var notCalled: Boolean = native
  var calledOnce: Boolean = native
  var called: Boolean = native
  def returns(args: Any*): Sinon = native
  def calledWith(args: Any*): Boolean = native
}
