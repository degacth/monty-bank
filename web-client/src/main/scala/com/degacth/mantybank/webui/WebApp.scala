package com.degacth.mantybank.webui

import org.scalajs.dom

trait WebApp extends scala.scalajs.js.JSApp {
  def main(): Unit = {
    init(dom.document.querySelector("#app"))
  }

  def init(container: dom.Element): Unit
}
