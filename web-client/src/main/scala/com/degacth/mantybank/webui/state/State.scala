package com.degacth.mantybank.webui.state

import com.degacth.montybank.messages.ConnectionMessage
import com.degacth.montybank.messages.ConnectionMessagesOpts._
import org.scalajs.dom

trait State

trait StateManager {
  protected val state: State
}

trait WsManager {
  val url: String
  private var ws: dom.WebSocket = _

  def onMessage(msg: ConnectionMessage): Unit

  def init(): Unit = {
    ws = new dom.WebSocket(url)
    ws.onmessage = (e: dom.MessageEvent) => {
      val json: String = e.data.asInstanceOf[String]

      json.fromStr match {
        case Right(value) => onMessage(value)
        case Left(error) => throw error
      }
    }
  }

  def send(message: ConnectionMessage): Unit = ws.send(message.toStr)
}
