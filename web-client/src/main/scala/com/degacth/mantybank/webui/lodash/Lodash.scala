package com.degacth.mantybank.webui.lodash

import scala.scalajs.js.annotation.JSGlobal
import scalajs.js._


@native
@JSGlobal("_")
object __ extends Object {
  def get[T, V](obj: T, path: Any, default: V = undefined): V = native
  def set[T](obj: T, path: String, value: Any): Unit = native
  def unset[T](obj: T, path: Array[String]): T = native
  def create[T](proto: T): T = native
  def noop(n: Any*): Unit = native
  def delay(f: Function, wait: Int, args: Any*): Int = native
  def clamp(n: Int, lower: Int, upper: Int): Int = native
}
