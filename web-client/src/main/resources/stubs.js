+function() {

if (typeof window === "undefined") return
if (!window.requestAnimationFrame) {
  window.requestAnimationFrame = () => null
  Vue.config.devtools = false
  Vue.config.productionTip = false
}

}();
