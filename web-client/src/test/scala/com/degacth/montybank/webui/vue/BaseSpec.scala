package com.degacth.montybank.webui.vue

import com.degacth.mantybank.webui.vue.ComponentSpec
import org.scalatest.BeforeAndAfterEach
import org.scalatest.wordspec.AsyncWordSpecLike

trait BaseSpec extends AsyncWordSpecLike with BeforeAndAfterEach with ComponentSpec {
  override def beforeEach(): Unit = {
    prepare()
    super.beforeEach()
  }
}
