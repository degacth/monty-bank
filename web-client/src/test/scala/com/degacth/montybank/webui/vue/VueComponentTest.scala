package com.degacth.montybank.webui.vue

import com.degacth.mantybank.webui.vue.BaseComponent
import org.scalajs.dom.html.Button

import scala.concurrent.{ExecutionContext, Future}
import scala.scalajs.js._

class VueComponentTest extends BaseSpec {
  override implicit val executionContext: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

  override def componentLiterals: (Object, Object) = (
    TestComponent, Dynamic.literal(testProp = new TestState)
  )

  def getButton: Button = component.$el.querySelector(".inc-button").asInstanceOf[Button]

  "Vue with Scala.js" must {
    "create typed component" in {
      assert(byCss(".test-prop").textContent === "4")
      assert(byCss(".test-var").textContent === "3")
      assert(byCss(".computed-var").textContent === "6")
    }

    "update data after event" in {
      byCss(".test-button").click()
      Future.unit map { _ =>
        assert(byCss(".test-prop").textContent === "12")
      }
    }
  }
}

class TestState extends Object {
  var intVal: Int = 2
  var strVal: String = "test-str"
}

abstract class TestComponent extends Object {
  var testProp: TestState
  var testVar: Int = _
}

object TestComponent extends BaseComponent {
  override val template: String =
    <div>
      <span class="test-prop">[[ testProp.intVal * 2 ]]</span>
      <span class="test-var">[[ testVar ]]</span>
      <span class="computed-var">[[ computed ]]</span>
      <button class="test-button" v-on:click="testProp.intVal += 4">button</button>
    </div> toString

  override val props: Array[String] = Array("testProp")
  override def data(): Dynamic = Dynamic.literal(
    testVar = 3,
  )

  override val computed: Dynamic = Dynamic.literal(
    computed = { (self: TestComponent) => self.testVar * self.testProp.intVal }: ThisFunction
  )
}
